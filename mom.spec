%{!?_pkgdocdir: %global _pkgdocdir %{_docdir}/%{name}-%{version}}
%{!?with_check: %global with_check 1}

%global		package_version 0.6.4
%global		package_name mom
Name:		%{package_name}
Version:	0.6.4
Release:	1
Summary:	Dynamically manage system resources on virtualization hosts

License:	GPLv2
URL:		https://www.ovirt.org
Source:		https://github.com/oVirt/%{name}/releases/download/v%{package_version}/%{name}-%{package_version}.tar.gz

Patch0001: 0001-Use-warning-method-instead-of-warn.patch

BuildArch:	noarch
BuildRequires:	python3-devel
BuildRequires:	python3-six

Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd
BuildRequires: systemd

# MOM makes use of libvirt by way of the python bindings to monitor and
# interact with virtual machines.
Requires:	libvirt-daemon-driver-qemu
Requires:	python3-libvirt
Requires:	python3-six
Requires:	procps


%description
MOM is a policy-driven tool that can be used to manage overcommitment on KVM
hosts. Using libvirt, MOM keeps track of active virtual machines on a host. At
a regular collection interval, data is gathered about the host and guests. Data
can come from multiple sources (eg. the /proc interface, libvirt API calls, a
client program connected to a guest, etc). Once collected, the data is
organized for use by the policy evaluation engine. When started, MOM accepts a
user-supplied overcommitment policy. This policy is regularly evaluated using
the latest collected data. In response to certain conditions, the policy may
trigger reconfiguration of the system’s overcommitment mechanisms. Currently
MOM supports control of memory ballooning and KSM but the architecture is
designed to accommodate new mechanisms such as cgroups.

%prep
%autosetup -n %{package_name}-%{package_version} -p1

%build
%configure \
        PYTHON="%{__python3}" \
        --docdir="%{_pkgdocdir}"
make %{?_smp_mflags}

%install
make DESTDIR="%{buildroot}" install

install -dm 755 %{buildroot}%{_unitdir}
install contrib/momd.service %{buildroot}%{_unitdir}
install -d -m 0755 "%{buildroot}/%{_sysconfdir}"
install -m 0644 doc/mom-balloon+ksm.conf "%{buildroot}/%{_sysconfdir}/momd.conf"

%check
%if 0%{with_check}
make check %{?_smp_mflags}
%endif

%post
%systemd_post momd.service

%preun
%systemd_preun momd.service

%postun
%systemd_postun_with_restart momd.service

%files
%config(noreplace) %{_sysconfdir}/momd.conf
%license COPYING
%doc README.md

%dir %{_pkgdocdir}/examples
%{_pkgdocdir}/examples/*
# COPYING is handled by license macro, avoid to ship duplicates
%exclude %{_pkgdocdir}/COPYING

%{_unitdir}/momd.service
%{_sbindir}/momd
%{python3_sitelib}/mom/

%changelog
* Tue Feb 20 2024 shafeipaozi <sunbo.oerv@isrc.iscas.ac.cn> - 0.6.4-1
- update version to 0.6.4

* Fri Mar 24 2023 wangdi <wangdi@kylinos.cn> - 0.6.2-3
- Set 'guest-manager-multi-thread' to 'true' in default configuration file

* Mon Mar 13 2023 wangdi <wangdi@kylinos.cn> - 0.6.2-2
- Fix wrong python3 intepreter path

* Tue Mar 15 2022 wangdi <wangdi@kylinos.cn> - 0.6.2-1
- Bump version to 0.6.2

* Tue Jul 06 2021 wangdi <wangdi@kylinos.cn> - 0.6.1-1
- Bump version to 0.6.1

* Wed Mar 04 2020 Huihui Fu <huihui.fu@cs2c.com.cn> - 0.5.12-1
- Initial package
